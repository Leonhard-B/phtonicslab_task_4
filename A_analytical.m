function result = A_analytical(L, t_r)
T_0 = 10e-12;   % [s]
%L = 10e3;        % [m]
gamma = 0;                  % [1]
D_2 = -1 * 1e-24 * 1e3;     % [s^2/km]

A_L = T_0 * (T_0^4 + 4 * D_2.^2 * L^2).^ 0.25;
T_L = T_0 * (1 + 4 * D_2.^2 / T_0^4 * L^2).^0.5;
result = A_L * exp(-t_r.^2 / (2 * T_L.^2));

end