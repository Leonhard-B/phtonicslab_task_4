%function task_3_1()
clear;

data.T = 1e-11;                       % [s]
data.N_t = 2048;
data.timesteps = linspace(-data.T, data.T, data.N_t);
data.tau = 1e-12;
data.gamma = 0;
data.D_2 = -1e-24 * 1e-3;  % [s^2/m]

data.z_lenght = 1e5;     % [m]
data.delta_z = 10;       % [m]
data.z = 0:data.delta_z:data.z_lenght;
data.z = data.z - data.delta_z/2;
data.z(1) = 0;
data.z(end+1) = data.z_lenght;


A_0 = sech(data.timesteps / data.tau);
plot(A_0)

%Do linear step with half delta z and nonlinear step with full delta z
A = linear_prop(data) .* fft(A_0); %XXXXXXXXXXXXXXX half delta z
A = ifft(nonlinear_prop (A, data));


%Repeat until end of waveguide
for run = 1:size(data.z,1)
    A_new_nonlin = ifft(nonlinear_prop(A, data));
    A = fft(A_new_nonlin .* linear_prop(data));
end

%Do nonlinear step with full delta z and step with half delta z


A_analytic = A_analytical(10e3 , data.timesteps);
%plot(data.timesteps, A_analytic)


%end

