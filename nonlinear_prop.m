function result = nonlinear_prop(A, data)
result = exp(1j * data.gamma * abs( A ).^2 / data.delta_z) .* A;

end