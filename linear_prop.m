function result = linear_prop(data)
delta_w = 2 * pi / (2 * data.T / data.N_t);
w = -(data.N_t - 0.5) * delta_w : 2 * delta_w : (data.N_t - 0.5) * delta_w;
result = exp(1j * data.D_2 * w.^2 * data.delta_z);
end